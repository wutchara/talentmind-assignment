package main

import 
(
	"fmt"  
	"net/http"
	"strings"
	"encoding/json"
	"errors"
	"io/ioutil"
) 

type googleRequest struct {
	Prefix string `json:"Category"`
	Suffix string `json:"City"`
}

const(
	ggKey = `AIzaSyBabtFKgjxm-3BKz1FAMpFbMoS4TIWOF2s`
)

func main(){
	port := `8888`
	fmt.Println("Server starting....")
	hamRoute()
	fmt.Println("Server start on port: "+port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		panic(err)
	}
}

func hamRoute(){
	http.HandleFunc("/", heltCheckCtrl)

	http.HandleFunc("/xyz", xyzSCGCtrl)
	http.HandleFunc("/google", googlePlaceSCGSearch)
}

func heltCheckCtrl(w http.ResponseWriter, r *http.Request) {
	fmt.Println("heltCheckCtrl....")
	message := r.URL.Path
	message = strings.TrimPrefix(message, "/")
	message = "HeltCheck " + message
	w.Write([]byte(message))
}

func googlePlaceSCGSearch(w http.ResponseWriter, r *http.Request){
	fmt.Println("googlePlaceSearch....")
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
		case "POST":
			var request googleRequest

			// receive request
			decoder := json.NewDecoder(r.Body)
			if err := decoder.Decode(&request); err != nil {
				internalError(w, err, http.StatusBadRequest)
			}else if request.Prefix == `` || request.Suffix == `` {
				internalError(w, errors.New(`Invalid input`), http.StatusBadRequest)
			}else if response, err := http.Get(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=`+request.Prefix+`+in+`+request.Suffix+`&key=`+ggKey); err != nil { // call google api
				internalError(w, err, http.StatusInternalServerError)
			} else if data, err := ioutil.ReadAll(response.Body); err != nil {
				internalError(w, err, http.StatusInternalServerError)
			}else{
				w.WriteHeader(http.StatusOK)
				w.Write(data)
			}
		default:
			permissionDenine(w)
	}
}

func xyzSCGCtrl(w http.ResponseWriter, r *http.Request) {
	fmt.Println("xyzCtrl....")

	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
		case "GET":
			ans_arr_count := 6
			ans_arr := []int{3}

			x := ``
			y := ``
			z := ``

			for i := 0; i < ans_arr_count; i++ {
				result := (i+1)*2 + ans_arr[i]
				ans_arr = append(ans_arr, result)
			}
			fmt.Println(ans_arr)

			z = fmt.Sprintf("%d", ans_arr[len(ans_arr)-1])
			y = fmt.Sprintf("%d", ans_arr[len(ans_arr)-2])
			x = fmt.Sprintf("%d", ans_arr[len(ans_arr)-3])

			ans := struct{
				X string
				Y string
				Z string
			}{
				x,
				y,
				z,
			}

			response := struct {
				Question string
				Ans interface{}
			}{
				`3, 5, 9, 15, X, Y, Z - Please create a new function for finding X, Y, Z value`,
				ans,
			}
			
			if js, err := json.Marshal(response); err == nil{
				w.WriteHeader(http.StatusOK)
				w.Write(js)
			}else{
				internalError(w, err, http.StatusInternalServerError)
			}
		default:
			permissionDenine(w)
	}
}

func permissionDenine(w http.ResponseWriter){
	w.WriteHeader(http.StatusForbidden)
	
	if js, err := json.Marshal(struct {
		Error string
	}{
		`Method not allow.`,
	}); err == nil{
		w.Write(js)
	}
}

func internalError(w http.ResponseWriter, err error, code int){
	w.WriteHeader(code)
	
	if js, err := json.Marshal(struct {
		Error string
	}{
		err.Error(),
	}); err == nil{
		w.Write(js)
	}
}