## TalentMind-ASSIGNMENT

First start server run on `http://127.0.0.1:8888`

```sh
$ go run main.go
```

## API (Back-end)

- Create a new controller called “SCG”
- 3, 5, 9, 15, X, Y, Z - Please create a new function for finding X, Y, Z value

| PATH | METHOD |
| ------ | ------ |
|`/xyz`|`GET`|

-----

- Please use “Place search|Place API(by Google)” for finding all restaurants in Bangsue area and show result by JSON

| PATH | METHOD |
| ------ | ------ |
|`/google`|`POST`|

Request: 
```JSON
{
    "Category": "restaurants",
    "City": "Bangkok"
}
```

Response: 
```JSON
{
"html_attributions": [ ],
"next_page_token": "=== TOKEN ===",
"results": [
        {
            "formatted_address": "Bangkok Marriott Marquis Queen’s Park, 199 ซอย สุขุมวิท 22 Khlong Ton, แขวงคลองเตย กรุงเทพมหานคร 10110 ประเทศไทย",
            "geometry": {},
            "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
            "id": "f88f43ffc90682d3cdcbc0391df0aa47dc892393",
            "name": "Siam Tea Room",
            "opening_hours": {},
            "photos": [],
            "place_id": "ChIJDf7QWt2e4jARJklxQGKyUgM",
            "plus_code": {},
            "rating": 4.6,
            "reference": "ChIJDf7QWt2e4jARJklxQGKyUgM",
            "types": [],
            "user_ratings_total": 157
        }
    ]
}
```

-----

- Please create one small project for Line messaging API(Up to you), contain a flow diagram, your code, and database.

- See [flow diagram]()