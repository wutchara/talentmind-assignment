import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Xyz from '@/components/Xyz'
import Api from '@/components/Api'
import Resume from '@/components/Resume'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/xyz',
      name: 'XYZ',
      component: Xyz,
    },{
      path: '/api',
      name: 'API',
      component: Api
    },{
      path: '/resume',
      name: 'Resume',
      component: Resume
    }
  ]
})
